package handler

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"git.capsulelab.eu/albandewilde/mice/model"
)

// DbHandlers regroup handlers that use the database
type DbHandlers struct {
	DB *sql.DB
}

// ListMice list all presents mouse
func (dbh *DbHandlers) ListMice(w http.ResponseWriter, r *http.Request) {
	// Allow only get request
	if r.Method != "GET" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	// Fetch all mice
	mice, err := model.Mice(dbh.DB)
	if err != nil {
		http.Error(w, "Failed fetching mice", http.StatusInternalServerError)
	}

	// Jsonify the list of mice
	jsnMice, err := json.Marshal(mice)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	// Sent the list of mice
	w.Write(jsnMice)
}

// ManipulateMouse apply action on mouse
func (dbh *DbHandlers) ManipulateMouse(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		dbh.getMouse(w, r)
	case "PUT":
		dbh.updateMouse(w, r)
	case "POST":
		dbh.createMouse(w, r)
	case "DELETE":
		dbh.deleteMouse(w, r)
	default:
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
	}
}

func (dbh *DbHandlers) createMouse(w http.ResponseWriter, r *http.Request) {
	// Check if the body isn't nil
	if r.Body == nil {
		http.Error(w, "No body given", http.StatusBadRequest)
		return
	}

	// Read the body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Error reading the body", http.StatusInternalServerError)
		return
	}

	// Create the mouse
	var m model.Mouse
	err = json.Unmarshal(body, &m)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Save the mouse
	m.Save(dbh.DB)

	// Send the mouse
	jsnMouse, err := json.Marshal(m)
	w.Write(jsnMouse)
}

func (dbh *DbHandlers) updateMouse(w http.ResponseWriter, r *http.Request) {
	// Check if the body isn't nil
	if r.Body == nil {
		http.Error(w, "No body given", http.StatusBadRequest)
		return
	}

	// Read the body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Error reading the body", http.StatusInternalServerError)
		return
	}

	// Get the mouse
	var m model.Mouse
	err = json.Unmarshal(body, &m)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Check if the mouse exist
	_, err = model.Get(dbh.DB, int(m.ID))
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	// Update the mouse
	err = m.Update(dbh.DB)
	if err != nil {
		http.Error(w, "Error updating the mouse", http.StatusInternalServerError)
		return
	}

	fmt.Fprint(w, "Ok")
}

func (dbh *DbHandlers) getMouse(w http.ResponseWriter, r *http.Request) {
	mouseID, msg, status := getIDInURL(r)
	if status != http.StatusOK {
		http.Error(w, msg, status)
		return
	}

	// Fetch the mouse
	m, err := model.Get(dbh.DB, int(mouseID))
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	jsnMouse, err := json.Marshal(m)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	// Send the mouse
	w.Write(jsnMouse)
}

func (dbh *DbHandlers) deleteMouse(w http.ResponseWriter, r *http.Request) {
	mouseID, msg, status := getIDInURL(r)
	if status != http.StatusOK {
		http.Error(w, msg, status)
		return
	}

	// Check if the mouse exist
	_, err := model.Get(dbh.DB, int(mouseID))
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	// Delete the mouse
	err = model.Delete(dbh.DB, int(mouseID))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	fmt.Fprint(w, "Deleted")
}

func getIDInURL(r *http.Request) (int64, string, int) {
	// Get the mouse id
	id := strings.TrimPrefix(r.URL.Path, "/mouse/")

	// Check if the mouse id is given
	if id == "" {
		return 0, "No mouse id", http.StatusBadRequest
	}

	// Check if the given id is an int
	mouseID, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		return 0, fmt.Sprintf("Error parsing the mouse id: %s", err.Error()), http.StatusBadRequest
	}

	return mouseID, "", http.StatusOK
}
