package handler

import (
	"database/sql"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	_ "github.com/mattn/go-sqlite3"

	"git.capsulelab.eu/albandewilde/mice/model"
)

const dbFileName = "db-test.sqlite3"

func TestNewMouse(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest(
		"POST",
		"http://localhost:8080/mouse/",
		strings.NewReader(`{"name": "Mini", "color": "black"}`),
	)
	handler := http.HandlerFunc(dbh.ManipulateMouse)

	handler.ServeHTTP(rr, r)

	expected := `{"id":2,"name":"Mini","color":"black"}`
	got := rr.Body.String()

	if status := rr.Code; status != http.StatusOK {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusOK,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestNewMouseWithEmptyBody(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest(
		"POST",
		"http://localhost:8080/mouse/",
		nil,
	)
	handler := http.HandlerFunc(dbh.ManipulateMouse)

	handler.ServeHTTP(rr, r)

	expected := `No body given`
	got := strings.TrimSpace(rr.Body.String())

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusBadRequest,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestNewMouseWithNoFields(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest(
		"POST",
		"http://localhost:8080/mouse/",
		strings.NewReader("{}"),
	)
	handler := http.HandlerFunc(dbh.ManipulateMouse)

	handler.ServeHTTP(rr, r)

	expected := `{"id":2,"name":"","color":""}`
	got := strings.TrimSpace(rr.Body.String())

	if status := rr.Code; status != http.StatusOK {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusOK,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestUpdateMouse(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest(
		"PUT",
		"http://localhost:8080/mouse/",
		strings.NewReader(`{"id": 1, "name": "Cookie", "color": "dark brown"}`),
	)
	handler := http.HandlerFunc(dbh.ManipulateMouse)

	handler.ServeHTTP(rr, r)

	expected := "Ok"
	got := strings.TrimSpace(rr.Body.String())

	if status := rr.Code; status != http.StatusOK {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusOK,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestUpdateNonExistingMouse(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest(
		"PUT",
		"http://localhost:8080/mouse/",
		strings.NewReader(`{"id": 115, "name": "Clarisse", "color": "pink"}`),
	)
	handler := http.HandlerFunc(dbh.ManipulateMouse)

	handler.ServeHTTP(rr, r)

	expected := "Mouse with id 115 is not present"
	got := strings.TrimSpace(rr.Body.String())

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusNotFound,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestUpdateMouseWithoutAnId(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest(
		"PUT",
		"http://localhost:8080/mouse/",
		strings.NewReader(`{"name": "Clarisse", "color": "pink"}`),
	)
	handler := http.HandlerFunc(dbh.ManipulateMouse)

	handler.ServeHTTP(rr, r)

	expected := "Mouse with id 0 is not present"
	got := strings.TrimSpace(rr.Body.String())

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusBadRequest,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestUpdateMouseWithNilBody(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest(
		"PUT",
		"http://localhost:8080/mouse/",
		nil,
	)
	handler := http.HandlerFunc(dbh.ManipulateMouse)

	handler.ServeHTTP(rr, r)

	expected := `No body given`
	got := strings.TrimSpace(rr.Body.String())

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusBadRequest,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestGetMouse(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest("GET", "http://localhost:8080/mouse/1", strings.NewReader(""))
	handler := http.HandlerFunc(dbh.ManipulateMouse)

	handler.ServeHTTP(rr, r)

	expected := `{"id":1,"name":"souris","color":"verte"}`
	got := rr.Body.String()

	if status := rr.Code; status != http.StatusOK {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusOK,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestGetNonExisingMouse(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest("GET", "http://localhost:8080/mouse/115", strings.NewReader(""))
	handler := http.HandlerFunc(dbh.ManipulateMouse)

	handler.ServeHTTP(rr, r)

	expected := "Mouse with id 115 is not present"
	got := strings.TrimSpace(rr.Body.String())

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusNotFound,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestGetNonIntegerMouseId(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest("GET", "http://localhost:8080/mouse/douze", strings.NewReader(""))
	handler := http.HandlerFunc(dbh.ManipulateMouse)

	handler.ServeHTTP(rr, r)

	expected := "Error parsing the mouse id: strconv.ParseInt: parsing \"douze\": invalid syntax"
	got := strings.TrimSpace(rr.Body.String())

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusBadRequest,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestDeleteMouse(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest("DELETE", "http://localhost:8080/mouse/1", strings.NewReader(""))
	handler := http.HandlerFunc(dbh.ManipulateMouse)

	handler.ServeHTTP(rr, r)

	expected := "Deleted"
	got := strings.TrimSpace(rr.Body.String())

	if status := rr.Code; status != http.StatusOK {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusOK,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestDeleteNonExistingMouse(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest("DELETE", "http://localhost:8080/mouse/115", strings.NewReader(""))
	handler := http.HandlerFunc(dbh.ManipulateMouse)

	handler.ServeHTTP(rr, r)

	expected := "Mouse with id 115 is not present"
	got := strings.TrimSpace(rr.Body.String())

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusNotFound,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestDeleteNonIntegerMouseId(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest("DELETE", "http://localhost:8080/mouse/douze", strings.NewReader(""))
	handler := http.HandlerFunc(dbh.ManipulateMouse)

	handler.ServeHTTP(rr, r)

	expected := "Error parsing the mouse id: strconv.ParseInt: parsing \"douze\": invalid syntax"
	got := strings.TrimSpace(rr.Body.String())

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusBadRequest,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestGetAllMice(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest("GET", "http://localhost:8080/mice/", strings.NewReader(""))
	handler := http.HandlerFunc(dbh.ListMice)

	handler.ServeHTTP(rr, r)

	expected := `[{"id":1,"name":"souris","color":"verte"}]`
	got := strings.TrimSpace(rr.Body.String())

	if status := rr.Code; status != http.StatusOK {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusOK,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func TestGetAllMiceWithWrongHTTPMeth(t *testing.T) {
	db := initializeDatabase()
	defer closeAndDelete(db)

	dbh := DbHandlers{db}

	rr := httptest.NewRecorder()
	r, _ := http.NewRequest("PUT", "http://localhost:8080/mice/", strings.NewReader(""))
	handler := http.HandlerFunc(dbh.ListMice)

	handler.ServeHTTP(rr, r)

	expected := http.StatusText(http.StatusMethodNotAllowed)
	got := strings.TrimSpace(rr.Body.String())

	if status := rr.Code; status != http.StatusMethodNotAllowed {
		t.Errorf(
			"Wrong status code.\nExpected: %d, but got: %d",
			http.StatusOK,
			status,
		)
	}

	if expected != got {
		t.Errorf("Expected: %s, bot got: %s", expected, got)
	}
}

func initializeDatabase() *sql.DB {
	// Create the sql3 database
	_, err := os.Create(dbFileName)
	if err != nil {
		log.Fatalf("Error creating the database file: %s", err.Error())
	}

	// Open the sql connection
	db, err := sql.Open("sqlite3", dbFileName)
	if err != nil {
		log.Fatalf("Error opening the database file: %s", err.Error())
	}

	// Create the table mice
	createTable := `create table mice (
            "id" integer not null primary key autoincrement,
			"name" text,
			"color" text
		);`
	statement, err := db.Prepare(createTable)
	if err != nil {
		log.Fatalf("Error preparing the statement to create the table: %s", err.Error())
	}
	_, err = statement.Exec()
	if err != nil {
		log.Fatalf("Error executing the statement to create the table: %s", err.Error())
	}

	// Insert mice
	insertMouse := "insert into mice(name, color) values (?, ?)"
	statement, err = db.Prepare(insertMouse)
	if err != nil {
		log.Fatalf("Error preparing the statement to insert mouse: %s", err.Error())
	}

	for _, m := range []model.Mouse{{Name: "souris", Color: "verte"}} {
		_, err = statement.Exec(m.Name, m.Color)
		if err != nil {
			log.Fatalf("Error executing the statement to insert a mouse: %s", err.Error())
		}
	}

	return db
}

func closeAndDelete(db *sql.DB) {
	db.Close()
	os.Remove(dbFileName)
}
