package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"

	_ "github.com/mattn/go-sqlite3"

	"git.capsulelab.eu/albandewilde/mice/handler"
	"git.capsulelab.eu/albandewilde/mice/model"
)

// DBNAME is the sqlite3 file that contain the database
const dbFileName = "db.sqlite3"

var dbh handler.DbHandlers

func main() {
	http.HandleFunc("/mouse/", dbh.ManipulateMouse)
	http.HandleFunc("/mice/", dbh.ListMice)

	fmt.Println("The server is now listening on port 8080.")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func init() {
	// Create the sql3 database
	_, err := os.Create(dbFileName)
	if err != nil {
		log.Fatal("Error creating the database file", err.Error())
	}

	// Open the sql connection
	db, err := sql.Open("sqlite3", dbFileName)
	if err != nil {
		log.Fatal("Error opening the database file", err.Error())
	}

	// Create the table mice
	createTable := `create table mice (
            "id" integer not null primary key autoincrement,
			"name" text,
			"color" text
		);`
	statement, err := db.Prepare(createTable)
	if err != nil {
		log.Fatal("Error preparing the statement to create the table", err.Error())
	}
	_, err = statement.Exec()
	if err != nil {
		log.Fatal("Error executing the statement to create the table", err.Error())
	}

	// Insert mice
	insertMouse := "insert into mice(name, color) values (?, ?)"
	statement, err = db.Prepare(insertMouse)
	if err != nil {
		log.Fatal("Error preparing the statement to insert mouse", err.Error())
	}

	// Insert one mouse
	for _, m := range []model.Mouse{{Name: "souris", Color: "verte"}} {
		_, err = statement.Exec(m.Name, m.Color)
		if err != nil {
			log.Fatal("Error executing the statement to insert a mouse", err.Error())
		}
	}

	dbh.DB = db
}
