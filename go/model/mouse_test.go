package model

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"testing"

	_ "github.com/mattn/go-sqlite3"
)

const dbFileName = "db-test.sqlite3"

func TestMice(t *testing.T) {
	cases := [][]Mouse{
		{},
		{
			{1, "Gil", "white"},
		},
		{
			{1, "Mr. Jingles", "brown"},
			{2, "La souris", "verte"},
		},
	}

	for _, test := range cases {
		db := initializeDatabase(test)

		// Get back all mouse
		mice, err := Mice(db)

		// Check if we got expected results
		if err != nil {
			t.Errorf("Error getting mice: %s", err.Error())
		}
		if !equal(test, mice) {
			t.Errorf("Get wrong mice\nExpected: %v but got: %v", test, mice)
		}

		closeAndDelete(db)
	}
}

func TestSaveMouse(t *testing.T) {
	db := initializeDatabase([]Mouse{})
	defer closeAndDelete(db)

	mice := []Mouse{
		{
			Name:  "Geraldine",
			Color: "white",
		},
		{
			Name:  "Mimi",
			Color: "Grey",
		},
	}

	for i, m := range mice {
		// Saving a mouse shouldn't throw an error
		err := m.Save(db)
		if err != nil {
			t.Errorf("Error saving a mouse: %s", err.Error())
		}
		// Assert the mouse have his id changed
		if m.ID != int64(i+1) {
			t.Errorf("The mouse id haven't change.\nExpected: %d but got: %d", i+1, m.ID)
		}
	}
}

func TestUpdateMouse(t *testing.T) {
	db := initializeDatabase([]Mouse{{Name: "Tom Elvis Jedusor", Color: "black"}})
	defer closeAndDelete(db)

	m, _ := Get(db, 1)

	m.Name = "Lord Voldemort"
	m.Color = "grey"

	err := m.Update(db)
	if err != nil {
		t.Errorf("Error when updating a mouse: %s", err.Error())
	}

	got, _ := Get(db, 1)

	if m != got {
		t.Errorf("Error updating a mouse.\nExpected: %v, but got %v", m, got)
	}
}

func TestGetMouse(t *testing.T) {
	db := initializeDatabase([]Mouse{{Name: "Jerry", Color: "brown"}})
	defer closeAndDelete(db)
	expected := Mouse{1, "Jerry", "brown"}

	got, err := Get(db, 1)
	if err != nil {
		t.Errorf("Error gtting the mouse: %s.", err.Error())
	}

	if got != expected {
		t.Errorf("Expected: %v, but got %v.", expected, got)
	}
}

func TestGetNonExistingMouse(t *testing.T) {
	db := initializeDatabase([]Mouse{})
	defer closeAndDelete(db)

	_, err := Get(db, 115)

	if err.Error() != fmt.Sprintf("Mouse with id %d is not present", 115) {
		t.Errorf("No error, or wrong error message %s", err.Error())
	}
}

func TestDeleteMouse(t *testing.T) {
	db := initializeDatabase([]Mouse{{Name: "Cookie", Color: "Brown"}})
	defer closeAndDelete(db)

	err := Delete(db, 1)
	if err != nil {
		t.Errorf("Error deleting mouse: %s", err.Error())
	}

	_, err = Get(db, 1)
	if err.Error() != "Mouse with id 1 is not present" {
		t.Errorf("No error returned of wrong error message: %s", err.Error())
	}

}

func TestDeleteNonExistingMouse(t *testing.T) {
	db := initializeDatabase([]Mouse{})
	defer closeAndDelete(db)

	err := Delete(db, 115)

	if err.Error() != "Mouse with id 115 is not present" {
		t.Errorf("No error returned of wrong error message: %s", err.Error())
	}
}

func equal(expected, got []Mouse) bool {
	if len(expected) != len(got) {
		return false
	}

	for i := range expected {
		if expected[i] != got[i] {
			return false
		}
	}
	return true
}

func initializeDatabase(mice []Mouse) *sql.DB {
	// Create the sql3 database
	_, err := os.Create(dbFileName)
	if err != nil {
		log.Fatalf("Error creating the database file: %s", err.Error())
	}

	// Open the sql connection
	db, err := sql.Open("sqlite3", dbFileName)
	if err != nil {
		log.Fatalf("Error opening the database file: %s", err.Error())
	}

	// Create the table mice
	createTable := `create table mice (
            "id" integer not null primary key autoincrement,
			"name" text,
			"color" text
		);`
	statement, err := db.Prepare(createTable)
	if err != nil {
		log.Fatalf("Error preparing the statement to create the table: %s", err.Error())
	}
	_, err = statement.Exec()
	if err != nil {
		log.Fatalf("Error executing the statement to create the table: %s", err.Error())
	}

	// Insert mice
	insertMouse := "insert into mice(name, color) values (?, ?)"
	statement, err = db.Prepare(insertMouse)
	if err != nil {
		log.Fatalf("Error preparing the statement to insert mouse: %s", err.Error())
	}

	for _, m := range mice {
		_, err = statement.Exec(m.Name, m.Color)
		if err != nil {
			log.Fatalf("Error executing the statement to insert a mouse: %s", err.Error())
		}
	}

	return db
}

func closeAndDelete(db *sql.DB) {
	db.Close()
	os.Remove(dbFileName)
}
