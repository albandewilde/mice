package model

import (
	"database/sql"
	"fmt"
)

// Mouse model the mouse in the db
type Mouse struct {
	ID    int64  `json:"id"`
	Name  string `json:"name"`
	Color string `json:"color"`
}

const insertMouse = `insert into mice (name, color) values (?, ?) returning id`

// Save a mouse
func (m *Mouse) Save(db *sql.DB) error {
	statement, err := db.Prepare(insertMouse)
	if err != nil {
		return err
	}

	result, err := statement.Exec(m.Name, m.Color)
	if err != nil {
		return err
	}

	id, _ := result.LastInsertId()

	m.ID = id

	return nil
}

const updateMouse = `update mice set name = ?, color = ? where id = ?`

// Update a mouse
func (m *Mouse) Update(db *sql.DB) error {
	statement, err := db.Prepare(updateMouse)
	if err != nil {
		return err
	}

	_, err = statement.Exec(m.Name, m.Color, m.ID)
	if err != nil {
		return err
	}

	return nil
}

const selectMouse = `select * from mice where id = ?`

// Get a mouse
func Get(db *sql.DB, id int) (Mouse, error) {
	statement, err := db.Prepare(selectMouse)
	if err != nil {
		return Mouse{}, err
	}

	rows, err := statement.Query(id)
	defer rows.Close()
	if err != nil {
		return Mouse{}, err
	}

	var m Mouse
	if !rows.Next() {
		return Mouse{}, fmt.Errorf("Mouse with id %d is not present", id)
	}

	err = rows.Scan(&m.ID, &m.Name, &m.Color)
	if err != nil {
		return Mouse{}, err
	}

	return m, nil
}

const listMice = `select * from mice`

// Mice return the list of all mouse
func Mice(db *sql.DB) ([]Mouse, error) {
	rows, err := db.Query(listMice)
	defer rows.Close()
	if err != nil {
		return nil, err
	}

	mice := make([]Mouse, 0)
	for rows.Next() {
		var m Mouse
		err := rows.Scan(&m.ID, &m.Name, &m.Color)

		if err != nil {
			return nil, err
		}

		mice = append(mice, m)
	}

	return mice, nil
}

const deleteMouse = `delete from mice where id = ?`

// Delete a mouse
func Delete(db *sql.DB, id int) error {
	_, err := Get(db, id)
	if err != nil {
		return err
	}

	statement, err := db.Prepare(deleteMouse)
	if err != nil {
		return err
	}

	_, err = statement.Exec(id)
	return err
}
