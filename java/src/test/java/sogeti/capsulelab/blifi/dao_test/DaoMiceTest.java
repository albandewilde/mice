package sogeti.capsulelab.blifi.dao_test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import sogeti.capsulelab.blifi.bean.Mouse;
import sogeti.capsulelab.blifi.repository.postgresql.mouse.IMouseDAO;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@SpringBootTest
@RunWith(SpringRunner.class)
@Transactional
public class DaoMiceTest {
    @Autowired
    NamedParameterJdbcOperations jdbcTemplate;

    @Autowired
    IMouseDAO mousedao;

    public void resetIdxOfMouseTable() {
        jdbcTemplate.update("delete from mice", Map.of());
        jdbcTemplate.update("delete from sqlite_sequence where name='mice'", Map.of());
    }

    @Test
    public void createMouse() {
        resetIdxOfMouseTable();

        // Make a mouse
        Mouse m = new Mouse(115,"Gil", "white");

        // Save the mouse
        mousedao.save(m);

        // Get back the saved mouse
        Mouse saved = mousedao.get(1);

        assertEquals(new Integer(1), saved.getId());
        assertEquals(new Integer(1), saved.getId());
        assertEquals(new Integer(1), saved.getId());
        assertEquals(m.getName(), saved.getName());
        assertEquals(m.getColor(), saved.getColor());
    }

    @Test
    public void updateMouse() {
        resetIdxOfMouseTable();

        // Make a mouse, save it and get it back
        mousedao.save(new Mouse(1, "Dorinette", "purple"));
        Mouse dorinette = mousedao.get(1);

        Mouse george = new Mouse(dorinette.getId(), "George", "pink");
        mousedao.update(george);

        Mouse got = mousedao.get(1);

        assertEquals(george.getId(), got.getId());
        assertEquals(george.getName(), got.getName());
        assertEquals(george.getColor(), got.getColor());
    }

    @Test
    public void getMouse() {
        resetIdxOfMouseTable();

        // Insert a mouse
        mousedao.save(new Mouse(1, "chuchu", "blue"));

        Mouse chuchu = mousedao.get(1);

        assertEquals(new Integer(1), chuchu.getId());
        assertEquals("chuchu", chuchu.getName());
        assertEquals("blue", chuchu.getColor());
    }

    @Test
    public void getNonExistingMouse() {
        resetIdxOfMouseTable();

        try {
            mousedao.get(115);
            fail("No exception raised");
        } catch (Error e) {
            assertEquals("Mouse with id 115 is not present", e.getMessage());
        }
    }

    @Test
    public void listMice() {
        resetIdxOfMouseTable();

        // Insert some mice
        mousedao.save(new Mouse(1, "a", "a"));
        mousedao.save(new Mouse(1, "b", "b"));
        mousedao.save(new Mouse(1, "c", "c"));

        List<Mouse> mice = mousedao.list();

        assertEquals(3, mice.size());
    }

    @Test
    public void deleteMouse() {
        resetIdxOfMouseTable();

        // Insert a mouse
        mousedao.save(new Mouse(1, "chuchu", "blue"));

        mousedao.delete(1);

        try {
            mousedao.get(1);
            fail("No exception raised");
        } catch (Error e) {
            assertEquals("Mouse with id 1 is not present", e.getMessage());
        }
    }

    @Test
    public void deleteNonExistingMouse() {
        resetIdxOfMouseTable();

        try{
            mousedao.delete(115);
            fail("No exception raised");
        } catch (Error e) {
            assertEquals("Mouse with id 115 is not present", e.getMessage());
        }
    }
}