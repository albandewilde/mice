package sogeti.capsulelab.blifi.controller_test;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sogeti.capsulelab.blifi.bean.Mouse;
import sogeti.capsulelab.blifi.controller.MouseController;
import sogeti.capsulelab.blifi.repository.postgresql.mouse.IMouseDAO;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
public class MouseControllerTest {
    @Mock
    IMouseDAO mousedao;
    @InjectMocks
    MouseController mousectrl;

    @Test
    public void newMouse() {
        Mouse m = new Mouse(1, "Mini", "black");
        when(mousedao.save(m)).thenReturn(12);

        Mouse got = mousectrl.newMouse(m);

        assertEquals(new Integer(12), got.getId());
        assertEquals("Mini", got.getName());
        assertEquals("black", got.getColor());
    }

    @Test
    public void updateMouse() {
        Mouse m = new Mouse(4,"Tom", "grey");
        when(mousedao.get(4)).thenReturn(new Mouse(4, "", ""));

        assertEquals("Ok", mousectrl.update(m));
    }

    @Test
    public void updateNonExistingMouse() {
        Mouse m = new Mouse(4,"Tom", "grey");
        when(mousedao.get(4)).thenThrow(new Error("Mouse with id 4 is not present"));

        try {
            mousectrl.update(m);
            fail("No exception raised");
        } catch (Exception e) {
            assertEquals("404 NOT_FOUND \"Mouse with id 4 is not present\"", e.getMessage());
        }
    }

    @Test
    public void getMouse() {
        Mouse m = new Mouse(4,"Tom", "grey");
        when(mousedao.get(4)).thenReturn(m);

        assertEquals(m, mousectrl.get(4));
    }

    @Test
    public void getNonExistingMouse() {
        Mouse m = new Mouse(4,"Tom", "grey");
        when(mousedao.get(4)).thenThrow(new Error("Mouse with id 4 is not present"));

        try {
            mousectrl.get(4);
            fail("No exception raised");
        } catch (Exception e) {
            assertEquals("404 NOT_FOUND \"Mouse with id 4 is not present\"", e.getMessage());
        }
    }

    @Test
    public void deleteMouse() {
        assertEquals("Deleted", mousectrl.delete(4));
    }

    @Test
    public void deleteNonExistingMouse() {
        when(mousedao.get(4)).thenThrow(new Error("Mouse with id 4 is not present"));

        try {
            mousectrl.delete(4);
            fail("No exception raised");
        } catch (Exception e) {
            assertEquals("404 NOT_FOUND \"Mouse with id 4 is not present\"", e.getMessage());
        }
    }

    @Test
    public void getAllMice() {
        when(mousedao.list()).thenReturn(List.of(new Mouse(1, "a", "b"), new Mouse(2, "c", "d")));

        List<Mouse> mice = mousectrl.list();

        assertEquals(2, mice.size());
    }
}