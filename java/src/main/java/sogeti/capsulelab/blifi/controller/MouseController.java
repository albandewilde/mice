package sogeti.capsulelab.blifi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import sogeti.capsulelab.blifi.bean.Mouse;
import sogeti.capsulelab.blifi.repository.postgresql.mouse.IMouseDAO;

import java.util.List;

@CrossOrigin
@RestController
public class MouseController {
    @Autowired
    IMouseDAO mouseDAO;

    @PostMapping("/mouse/")
    public Mouse newMouse(@RequestBody Mouse m) {
        Integer id = mouseDAO.save(m);
        return new Mouse(id, m.getName(), m.getColor());
    }

    @PutMapping("/mouse/")
    public String update(@RequestBody Mouse m) {
        try {
            mouseDAO.get(m.getId());
        } catch (Error e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage()
            );
        }

        mouseDAO.update(m);

        return "Ok";
    }

    @GetMapping("/mouse/{id}")
    public Mouse get(@PathVariable Integer id) {
        Mouse m;

        try {
            m = mouseDAO.get(id);
        } catch (Error e) {
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND,
                e.getMessage()
            );
        }

        return m;
    }

    @DeleteMapping("/mouse/{id}")
    public String delete(@PathVariable Integer id) {
        try {
            mouseDAO.get(id);
        } catch (Error e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage()
            );
        }

        mouseDAO.delete(id);

        return "Deleted";
    }

    @GetMapping("/mice/")
    public List<Mouse> list() {
        return mouseDAO.list();
    }
}