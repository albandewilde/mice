package sogeti.capsulelab.blifi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.sql.DriverManager;
import java.sql.SQLException;

@SpringBootApplication
public class BackEndMainApplication extends WebMvcConfigurerAdapter {

	public static void main(String[] args) {
	    createNewDB();
		SpringApplication.run(BackEndMainApplication.class, args);
	}

	public static void createNewDB() {

		String url = "jdbc:sqlite:./db.sqlite3";

		try {
			DriverManager.getConnection(url);
		} catch (SQLException e) {
			throw new Error(e.getMessage());
		}
	}
}