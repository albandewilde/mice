package sogeti.capsulelab.blifi.repository.postgresql.mouse;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import sogeti.capsulelab.blifi.bean.Mouse;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class MouseRowMapper implements RowMapper<Mouse> {
    @Override
    public Mouse mapRow(ResultSet rs, int rowNum) throws SQLException {
        Mouse m = new Mouse(
            rs.getInt("id"),
            rs.getString("name"),
            rs.getString("color")
        );

        return m;
    }
}
