package sogeti.capsulelab.blifi.repository.postgresql.mouse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Service;
import sogeti.capsulelab.blifi.bean.Mouse;

import java.util.List;
import java.util.Map;

@Service
public class MouseDAOImpl implements IMouseDAO {
    @Autowired
    NamedParameterJdbcOperations jdbcTemp;

    @Autowired
    MouseRowMapper mousempr;

    @Override
    public Integer save(Mouse mouse) {
        Map<String, Object> params = Map.of(
            "name", mouse.getName(),
            "color", mouse.getColor()
        );

        jdbcTemp.update(
            "insert into mice (name, color) values (:name, :color)",
            params
        );
        Integer id = jdbcTemp.queryForObject(
            "select max(id) from mice",
            Map.of(),
            Integer.class
        );

        return id;
    }

    @Override
    public void update(Mouse mouse) {
        jdbcTemp.update(
            "update mice set name = :name, color = :color where id = :id",
            Map.of(
                "name", mouse.getName(),
                "color", mouse.getColor(),
                "id", mouse.getId()
            )
        );
    }

    @Override
    public Mouse get(Integer id) {
        Mouse m;

        try {
            m = jdbcTemp.queryForObject(
                "select * from mice where id = :id",
                Map.of("id", id),
                mousempr
            );
        } catch (IncorrectResultSizeDataAccessException e) {
            throw new java.lang.Error(String.format("Mouse with id %d is not present", id));
        }

        return m;
    }

    @Override
    public List<Mouse> list() {
        return jdbcTemp.query("select * from mice", mousempr);
    }

    @Override
    public void delete(Integer id) {
        // Check if the mouse is present
        get(id);
        jdbcTemp.update("delete from mice where id = :id", Map.of("id", id));
    }
}