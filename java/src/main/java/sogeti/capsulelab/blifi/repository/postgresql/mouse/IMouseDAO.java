package sogeti.capsulelab.blifi.repository.postgresql.mouse;

import sogeti.capsulelab.blifi.bean.Mouse;

import java.util.List;

public interface IMouseDAO {
    Integer save(Mouse mouse);

    void update(Mouse mouse);

    Mouse get(Integer id);

    List<Mouse> list();

    void delete(Integer id);
}
