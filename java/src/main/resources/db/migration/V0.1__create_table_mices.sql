create table mice (
    id     integer    not null    primary key    autoincrement,
    name   text       not null,
    color  text       not null
);